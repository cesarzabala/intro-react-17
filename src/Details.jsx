import { useParams } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import Carousel from "./Carousel";
import ThemeContext from "./ThemeContext";
import Modal from "./Modal";

const Details = () => {
  const { id } = useParams();

  const [loading, setLoading] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [detail, setDetail] = useState({});
  const [theme] = useContext(ThemeContext);

  const toggleModal = () => setShowModal(!showModal);

  useEffect(() => {
    requestDetail();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  async function requestDetail() {
    const res = await fetch(`http://pets-v2.dev-apis.com/pets?id=${id}`);
    const json = await res.json();
    setDetail(json.pets[0]);
    setLoading(false);
  }

  const { animal, breed, city, state, description, name, images } = detail;

  return (
    <>
      {loading ? (
        <h1>Loading...</h1>
      ) : (
        <div className="details">
          <Carousel images={images} />
          <div>
            <h1>{name}</h1>
            <h2>{`${animal} — ${breed} — ${city}, ${state}`}</h2>
            <button style={{ backgroundColor: theme }} onClick={toggleModal}>
              Adopt {name}
            </button>
            <p>{description}</p>
            {showModal ? (
              <Modal>
                <div>
                  <h1>Would you like to adopt {name}</h1>
                </div>
                <button onClick={toggleModal}>No</button>
              </Modal>
            ) : null}
          </div>
        </div>
      )}
    </>
  );
};

export default Details;
