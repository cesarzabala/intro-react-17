import { useState, useEffect, useDebugValue } from "react";

const localCache = {};

export default function useBreedList(animal) {
  const [breedList, setBreedList] = useState([]);
  const [status, setStatus] = useState("unloaded");
  const [error, setError] = useState("");

  useDebugValue(`USE STATUS --- ${status}`);

  useEffect(() => {
    if (!animal) {
      setBreedList([]);
    } else if (localCache[animal]) {
      setBreedList(localCache[animal]);
    } else {
      requestBreedList();
    }

    async function requestBreedList() {
      setBreedList([]);
      setStatus("loading");

      try {
        const res = await fetch(
          `http://pets-v2.dev-apis.com/breeds?animal=${animal}`
        );

        const json = await res.json();
        localCache[animal] = json.breeds || [];
        setBreedList(localCache[animal]);
        setStatus("unloaded");
      } catch (err) {
        setStatus("error");
        setError(err.message);
      }
    }
  }, [animal]);

  return [breedList, status, error];
}
