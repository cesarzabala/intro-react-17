/* global React ReactDOM */

const Pet = (props) => {
  return React.createElement("div", {}, [
    React.createElement("h1", {}, props.name),
    React.createElement("h2", {}, props.animal),
    React.createElement("h2", {}, props.breed),
  ]);
};

let counter = 0;
const App = () => {
  counter++;
  return React.createElement("div", {}, [
    React.createElement("h1", { id: "brand" }, `Adopt Me! ${counter}`),
    React.createElement(Pet, {
      name: "Snow",
      animal: "Dog",
      breed: "Husky",
    }),
    React.createElement(Pet, {
      name: "Pepper",
      animal: "Bird",
      breed: "Cocktail",
    }),
    React.createElement(Pet, {
      name: "Doink",
      animal: "Cow",
      breed: "Mix",
    }),
  ]);
};

//React 17
ReactDOM.render(React.createElement(App), document.getElementById("root"));

// React 18
// const root = ReactDOM.createRoot(document.getElementById("root"))
// root.render(React.createElement(App))
